﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text.RegularExpressions;
using Microsoft.SharePoint.Client;

namespace Bank1
{
    class Program
    {
        static void Main(string[] args)
        {

            var pwdS = new SecureString();
            foreach (var c in "ReckittBen1".ToCharArray()) pwdS.AppendChar(c);
            var ctx = new ClientContext("https://rbcom.sharepoint.com/sites/CSOMTest") { Credentials = new SharePointOnlineCredentials("Sp10gammatest2@RBcom.onmicrosoft.com", pwdS) };
            var list = ctx.Web.Lists.GetByTitle("Account");
            
            Web web = ctx.Web;
            //ctx.Load(web.Lists,
            //lists => lists.Include(list => list.Title,
            //                     list => list.Id));
            var items = list.GetItems(new CamlQuery { ViewXml = ""});
            ctx.Load(items);
            ctx.ExecuteQuery();

            foreach(var item in items)
            {
                Console.WriteLine(item["Title"]);
            }


           // ListItemCreationInformation newItem = new ListItemCreationInformation();
           // ListItem listItem = list.AddItem(newItem);
           // listItem["Title"] = "SavingsAccount";
           // listItem["Accountsnumber"] = "94000000002";
           // listItem["Accountsbalance"] = "66.6";
           // listItem["Firstname"] = "Mojo";
           // listItem["Lastname"] = "Risin";
            //listItem["Pesel"] = "92010133333";
            //listItem.Update();
           // ctx.ExecuteQuery();

            foreach (ListItem item in items)
            {
                if (item.Id.ToString() =="1")
                {
                    item["Accountsnumber"] = "94000000001";
                    item.SystemUpdate();
                }
            }
            ctx.ExecuteQuery();

            ListItem deleteItem = list.GetItemById(4);
            deleteItem.DeleteObject();
            ctx.ExecuteQuery();


            string name = "Nazwa: Bank";
            string author = "Autor: Pamela";
            Console.WriteLine(name);
               Console.WriteLine(author);
               Console.WriteLine();



              SavingsAccount savingsAccount = new SavingsAccount(1, "94000000001", "Pamela", "Rose", 9201300456);

            string fullName = savingsAccount.GetFullName();
             Console.WriteLine("Pierwsze konto w systemie dostal (-a): {0}", fullName);

             SavingsAccount secondSavingsAccount = new SavingsAccount(2, "94000000002", "Jacek", "Rabbit", 92010133333);


             BillingAccount billingAccount = new BillingAccount(3, "94000000003", "William", "Plant", 92010133366);


             Printer printer = new Printer();

             printer.Print(savingsAccount);
                printer.Print(secondSavingsAccount);
                printer.Print(billingAccount);

            Console.ReadKey();
            
            
        }
    }
}